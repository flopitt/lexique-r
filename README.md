
# lexique

<!-- badges: start -->
<!-- badges: end -->

The goal of lexique is to simplify usage of https://www.lexique.org by adding wrapping it in a R object and give method to access its content more easily.

## Installation

You can install the released version of lexique from this repository with:

``` r
devtools::install_gitlab("flopitt/lexique-r", type="source")
```

## Example

Here is a simple example to load 8 letters words with most consonants

``` r
library(lexique)
lex <- lexique()
highestRankCV(lex, "C", 8)
```

